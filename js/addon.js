document.addEventListener('DOMContentLoaded', function() {
    var genericExamples = document.querySelectorAll('[data-trigger]');
    for (i = 0; i < genericExamples.length; ++i) {
        var element = genericExamples[i];
        new Choices(element, {
            allowHTML: true,

        });
    }

}); 
 
function toggleMenu() {
    var openMenu = document.getElementById("sidebar");
    openMenu.classList.toggle("toggled");
}